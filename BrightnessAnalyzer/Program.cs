﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace BrightnessAnalyzer {

	class Program {

		// Mapping typewriter character codes to ascii character codes (starts with 32 padding characters)
		// ("Typenrad Nr. 01, Ländervariante: Deutschland")
		private static readonly char[] CharMap = new char[] {
			'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
			'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
			'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
			'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0',
			' ', '!', '"', '#', '$', '%', '&', '´',
			'(', ')', '*', '+', ',', '-', '.', '/',
			'0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', ':', ';', '<', '=', '>', '?',
			'§', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
			'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
			'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
			'X', 'Y', 'Z', 'Ä', 'Ö', 'Ü', '^', '_',
			'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
			'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
			'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'ä', 'ö', 'ü', 'ß'
		};

		static void Main(string[] args) {
			// Get the image
			var image = Image.FromFile("..\\..\\..\\..\\code table DE.png");
			var bmp = (Bitmap) image;

			int w = image.Width, h = image.Height;
			int x, y, xTile, yTile, nWTile, nHTile, posX, posY, pixelsCovered, i, area;
			double fDarkest, fBright, wTile = w / 6.0, hTile = h / 16.0;

			// Calculate brightness of each letter
			var letters = new List<Tuple<byte, double>>(); // char code, brightness
			for (y = 0; y < 16; ++y) {
				for (x = 0; x < 6; ++x) {
					posX = (int) Math.Round(x * wTile);
					posY = (int) Math.Round(y * hTile);
					nWTile = (int) Math.Round((x + 1) * wTile) - posX;
					nHTile = (int) Math.Round((y + 1) * hTile) - posY;
					area = nWTile * nHTile;
					pixelsCovered = 0;

					for (yTile = posY; yTile < (posY + nHTile); ++yTile) {
						for (xTile = posX; xTile < (posX + nWTile); ++xTile) {
							if (bmp.GetPixel(xTile, yTile).GetBrightness() < 0.5) ++pixelsCovered;
						}
					}

					// Store letter information
					fBright = (area - pixelsCovered) / (double) area;
					byte c = (byte) ((x + 2) * 0x10 + y);
					c = (c == 0x7F) ? (byte) 0x20 : c;  // Replace DEL with SPACE
					letters.Add(new Tuple<byte, double>(c, fBright));
				}
			}

			// Sort letter informaton list from darkest to lightest
			letters.Sort((x, y) => { if (x.Item2 > y.Item2) return 1; if (x.Item2 < y.Item2) return -1; return 0; });

			// Replace '_' (underscore) with next character (underscore looks ugly in ASCII art)
			int iUnderscore = letters.FindIndex(c => CharMap[c.Item1] == '_');
			letters.RemoveAt(iUnderscore);
			letters.Insert(iUnderscore, letters[iUnderscore]);

			// Normalize brightness values to 0.0 - 1.0
			fDarkest = letters[0].Item2;
			for (i = 0; i < letters.Count; ++i) {
				var letter = letters[i];
				double fNormalized = (letter.Item2 - fDarkest) * (1 / (1 - fDarkest));
				letters[i] = new Tuple<byte, double>(letter.Item1, fNormalized);
			}

			// Output what has been determined so far
			var letterDesc = letters.Select(l => $"0x{l.Item1:X} / '{CharMap[l.Item1]}': {l.Item2:0.###}");
			Console.WriteLine($"Normalized letter brightness from darkest to lightest:\r\n{string.Join("\r\n", letterDesc)}\r\n");

			// Generate brightness (0 - 255) to letter lookup table
			var lookupTable = new byte[256];
			for (i = 0; i < 256; ++i) {
				fBright = i / 255.0;
				var nearest = letters.Aggregate((current, next) => Math.Abs(current.Item2 - fBright) < Math.Abs(next.Item2 - fBright) ? current : next);
				lookupTable[i] = nearest.Item1;
			}
			Console.WriteLine("Lookup table as a byte array which can be used in C, C#, Python etc:\r\n");
			Console.WriteLine($"[ {string.Join(", ", lookupTable.Select(c => $"0x{c:X2}"))} ]");
		}
	}
}
